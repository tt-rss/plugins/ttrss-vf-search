<?php
class VF_Search extends Plugin implements IVirtualFeed {

	/** @var PluginHost */
	private $host;

	/** @var array<int, array<int, int|string>> */
	private $feeds = [];

	function about() {
		return array(1.0,
			"Show searches as feeds",
			"fox",
			false);
	}

	function init($host) {
		$this->host = $host;

		$searches = explode("\n", $this->host->get($this, "searches", ""));

		foreach ($searches as $line) {
			if (strpos($line, ",") !== FALSE) {
				list ($caption, $query) = explode(",", $line, 2);

				if ($caption && $query)
					array_push($this->feeds,
						[$host->add_feed(-1, $caption, 'search', $this), $query]
					);
			}
		}

		$host->add_hook($host::HOOK_PREFS_TAB, $this);
	}

	function get_unread(int $feed_id) : int {
		return 0;
	}

	function get_total(int $feed_id) : int {
		return 0;
	}

	function get_headlines(int $feed_id, array $options) : array {
		$search = array_reduce($this->feeds,
			function ($carry, $feed) use ($feed_id) {
				if ($feed_id == $feed[0])
					return $feed[1];
				else
					return $carry;
			}
		);

		$params = array(
			"feed" => -4,
			"limit" => $options["limit"],
			"view_mode" => $this->get_unread(-1) > 0 ? "adaptive" : "all_articles",
			"override_order" => $options['override_order'],
			"offset" => $options["offset"],
			"filter" => $options["filter"],
			"since_id" => $options["since_id"],
			"include_children" => $options["include_children"],
			"search" => $search,
			"override_vfeed" => "ttrss_feeds.title AS feed_title,"
		);

		$qfh_ret = Feeds::_get_headlines($params);
		$qfh_ret[1] = "Search results: $search";

		return $qfh_ret;
	}

	function hook_prefs_tab($args) {
		if ($args != "prefFeeds") return;

		$searches = $this->host->get($this, "searches", "");

		?>
		<div dojoType='dijit.layout.AccordionPane'
			title="<i class='material-icons'>search</i> Show searches as feeds (vf_search)">

			<form dojoType='dijit.form.Form'>

				<?= \Controls\pluginhandler_tags($this, "save") ?>

				<script type="dojo/method" event="onSubmit" args="evt">
					evt.preventDefault();
					if (this.validate()) {
						xhrPost("backend.php", this.getValues(), (transport) => {
							Notify.info(transport.responseText);
						})
					}
				</script>

				<?= format_notice("One line per generated feed. Use the following format for each line: <b>feed title,search query</b>") ?>

				<fieldset>
					<textarea dojoType='dijit.form.SimpleTextarea' style='width : 500px' rows='4' name='searches'><?= $searches ?></textarea>
				</fieldset>

				<?= \Controls\submit_tag("Save") ?>

			</form>
		</div>
		<?php
	}

	function save() : void {
		$searches = ($_POST["searches"] ?? "");

		$this->host->set($this, "searches", $searches);

		echo "Data saved";
	}

	function api_version() {
		return 2;
	}

}
